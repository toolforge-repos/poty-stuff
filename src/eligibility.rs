use crate::config::{current_year, poty_state};
use crate::{commons_bot, CommonsDB, ToolsDB};
use anyhow::Result;
use mwbot::Bot;
use mysql_async::prelude::Queryable;
use std::collections::HashSet;
use tokio::time;

async fn is_eligible(
    bot: &Bot,
    username: &str,
    account_eligibility_id: usize,
) -> Result<bool> {
    let client = bot.api().http_client();
    let url =
        format!("https://meta.toolforge.org/accounteligibility/{account_eligibility_id}/{username}");
    println!("Fetching {url}...");
    let req = client.get(&url).build()?;
    let resp = client.execute(req).await?;
    let html = resp.error_for_status()?.text().await?;
    Ok(html.contains("data-is-eligible=\"1\""))
}

pub(crate) async fn eligible_voters_count(
    pool: &ToolsDB,
    year: usize,
) -> Result<usize> {
    let mut conn = pool.0.get_conn().await?;
    let result = conn
        .exec_first(
            r#"
SELECT COUNT(*)
FROM eligibility
WHERE e_year=?
AND e_eligible=true
"#,
            (year,),
        )
        .await?
        .unwrap();
    Ok(result)
}

pub(crate) async fn ineligible_voters(
    pool: &ToolsDB,
    year: usize,
) -> Result<HashSet<String>> {
    let mut conn = pool.0.get_conn().await?;
    let results = conn
        .exec(
            r#"
SELECT e_username
FROM eligibility
WHERE e_year=?
AND e_eligible=false
"#,
            (year,),
        )
        .await?
        .into_iter()
        .collect();
    Ok(results)
}

async fn update_user(
    pool: &ToolsDB,
    username: &str,
    year: usize,
    eligiblity: bool,
) -> Result<()> {
    let mut conn = pool.0.get_conn().await?;
    conn.exec_drop(
        r#"
INSERT INTO eligibility (e_username, e_year, e_eligible)
VALUES (?, ?, ?);
"#,
        (username, year, eligiblity),
    )
    .await?;
    Ok(())
}

async fn update_eligibility(
    toolsdb: &ToolsDB,
    commonsdb: &CommonsDB,
    bot: &Bot,
    year: usize,
    account_eligibility_id: usize,
) -> Result<()> {
    let known = known_status(toolsdb, year).await?;
    let all_voters = voter_list(commonsdb, year).await?;
    let mut count = 0;
    for voter in all_voters.difference(&known) {
        let eligiblity =
            is_eligible(bot, voter, account_eligibility_id).await?;
        update_user(toolsdb, voter, year, eligiblity).await?;
        count += 1;
    }
    println!("updated eligiblity status for {count} voters");
    Ok(())
}

/// Start a loop to run eligibility checking every hour
pub(crate) async fn start_eligiblity_checking(
    toolsdb: ToolsDB,
    commonsdb: CommonsDB,
) {
    let bot = commons_bot().await.expect("failed to create Bot");
    let (year, cfg) = current_year(
        poty_state().await.expect("failed to load POTY state.json"),
    );
    loop {
        match update_eligibility(
            &toolsdb,
            &commonsdb,
            &bot,
            year,
            cfg.eligibility.id,
        )
        .await
        {
            Ok(()) => {}
            Err(err) => {
                eprintln!("ERROR: {err}")
            }
        }
        time::sleep(time::Duration::from_secs(60 * 60)).await;
    }
}

/// List of voters we know eligibility status of
async fn known_status(pool: &ToolsDB, year: usize) -> Result<HashSet<String>> {
    let mut conn = pool.0.get_conn().await?;
    let result = conn
        .exec(
            r#"
select e_username
from eligibility
where e_year=?
"#,
            (year,),
        )
        .await?
        .into_iter()
        .collect();
    Ok(result)
}

/// List of all voters so far
async fn voter_list(pool: &CommonsDB, year: usize) -> Result<HashSet<String>> {
    let mut conn = pool.0.get_conn().await?;
    let title = format!("Picture_of_the_Year/{year}/R_/v/%");
    let result = conn
        .exec(
            r#"
select DISTINCT lt_title
from pagelinks
join linktarget on pl_target_id=lt_id
join page on pl_from=page_id
where page_namespace=4
and page_title like ?
and lt_namespace=2
and pl_from_namespace=4;
"#,
            (title,),
        )
        .await?
        .into_iter()
        .collect();
    Ok(result)
}
