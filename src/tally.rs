use crate::guard::UserGuard;
use crate::{CommonsDB, ConsoleSender, ToolsDB};
use anyhow::Result;
use mwbot::parsoid::{ImmutableWikicode, WikinodeIterator};
use mwbot::{Bot, SaveOptions};
use mysql_async::prelude::Queryable;
use rand::seq::SliceRandom;
use rand::thread_rng;
use serde::Serialize;
use std::collections::{BTreeSet, HashMap, HashSet};

#[derive(Serialize)]
pub(crate) struct R1Outcome {
    name: String,
    category: String,
    total_votes: usize,
    counted_votes: usize,
    ineligible: BTreeSet<String>,
    overall_rank: usize,
    cat_rank: usize,
    advances: bool,
}

pub(crate) async fn tally_r1(
    commonsdb: &CommonsDB,
    toolsdb: &ToolsDB,
    user: &UserGuard,
    logger: &ConsoleSender,
    year: usize,
) -> Result<Vec<R1Outcome>> {
    let bot = user.bot().await?;
    let page =
        bot.page(&format!("Commons:Picture of the Year/{year}/Candidates"))?;
    let html = page.html().await?;
    let mut by_categories = parse_candidates(html, &bot);
    let ineligible =
        crate::eligibility::ineligible_voters(toolsdb, year).await?;
    let mut votes = query_votes(commonsdb, year, 1).await?;
    // Step 1: Assign vote counts, remove ineligible voters
    for outcomes in by_categories.values_mut() {
        for outcome in outcomes {
            if let Some(voters) = votes.remove(&outcome.name) {
                outcome.total_votes = voters.len();
                // Counted votes is total minus ineligible
                outcome.counted_votes = voters.difference(&ineligible).count();
                // Keep track of the ineligible voters who were ignored
                outcome.ineligible =
                    voters.intersection(&ineligible).cloned().collect();
            }
        }
    }
    if !votes.is_empty() {
        let names = votes.into_keys().collect::<Vec<_>>().join(", ");
        let _ =
            logger.send(user.console(&format!(
                "Error: some votes were not tallied: {names}"
            )));
    }
    // Step 2: Assign category ranks
    for outcomes in by_categories.values_mut() {
        let values: HashSet<usize> = outcomes
            .iter()
            .map(|outcome| outcome.counted_votes)
            .collect();
        let mut values: Vec<_> = values.into_iter().collect();
        values.sort();
        values.reverse();
        // We now have a vec of vote totals in descending order. We want
        // to assign ranks in order, but if two images have the same rank
        // e.g. 1 and 1, then the next value should be assigned 3.
        // We track the rank for reach round, and count how many images get
        // assigned that rank (offset). Then we increment rank by offset
        // and move to the next set.
        let mut rank: usize = 1;
        for value in values {
            let mut offset = 0;
            for outcome in &mut *outcomes {
                if outcome.counted_votes == value {
                    // Assign rank
                    outcome.cat_rank = rank;
                    offset += 1;
                }
            }
            rank += offset;
        }
    }
    // Step 3: Flatten and assign overall ranks
    let mut outcomes: Vec<_> = by_categories.into_values().flatten().collect();
    let values: HashSet<usize> = outcomes
        .iter()
        .map(|outcome| outcome.counted_votes)
        .collect();
    let mut values: Vec<_> = values.into_iter().collect();
    values.sort();
    values.reverse();
    let mut rank: usize = 1;
    for value in values {
        let mut offset = 0;
        for outcome in &mut outcomes {
            if outcome.counted_votes == value {
                // Assign rank
                outcome.overall_rank = rank;
                offset += 1;
            }
        }
        rank += offset;
    }
    // Step 4: Mark advancement
    for outcome in &mut outcomes {
        if outcome.overall_rank <= 30 || outcome.cat_rank <= 2 {
            outcome.advances = true;
        }
    }
    // Step 5: Sort by total votes
    outcomes.sort_by_key(|outcome| outcome.counted_votes);
    outcomes.reverse();
    Ok(outcomes)
}

pub(crate) fn format_wikitext(
    outcomes: &[R1Outcome],
    year: usize,
    account_eligibility: usize,
) -> String {
    let mut ret = vec![];
    for outcome in outcomes {
        let mut line = format!(
            "#{}: [[Commons:Picture of the Year/{year}/R1/v/{}|{}]] ({} votes); #{} in {}",
            outcome.overall_rank,
            outcome.name,
            outcome.name.replace('_', " "),
            outcome.counted_votes,
            outcome.cat_rank,
            outcome.category,
        );
        if !outcome.ineligible.is_empty() {
            line.push_str(&format!("<br>Discounted votes by ineligible users (had {} total votes):", outcome.total_votes));
            for voter in &outcome.ineligible {
                line.push_str(&format!(
                    "\n** [[Special:Contributions/{voter}|{voter}]] ([https://meta.toolforge.org/accounteligibility/?event={account_eligibility}&user={voter} AccountEligibility])"
                ));
            }
        }
        if outcome.advances {
            line =
                format!("<div style=\"background: lightblue;\">{line}</div>");
        }
        line = format!("* {line}");
        ret.push(line);
    }

    ret.join("\n")
}

pub(crate) fn format_wikitext2(
    outcomes: &[R2Outcome],
    year: usize,
    account_eligibility: usize,
) -> String {
    let mut ret = vec![];
    for outcome in outcomes {
        let mut line = format!(
            "#{}: [[Commons:Picture of the Year/{year}/R2/v/{}|{}]] ({} votes);",
            outcome.overall_rank,
            outcome.name,
            outcome.name.replace('_', " "),
            outcome.counted_votes,
        );
        if !outcome.ineligible.is_empty() {
            line.push_str(&format!("<br>Discounted votes by ineligible users (had {} total votes):", outcome.total_votes));
            for voter in &outcome.ineligible {
                line.push_str(&format!(
                    "\n** [[Special:Contributions/{voter}|{voter}]] ([https://meta.toolforge.org/accounteligibility/?event={account_eligibility}&user={voter} AccountEligibility])"
                ));
            }
        }
        line = format!("* {line}");
        ret.push(line);
    }

    ret.join("\n")
}

fn format_gallery(outcomes: &[R1Outcome], year: usize) -> String {
    let mut text = vec![];
    for outcome in outcomes {
        if !outcome.advances {
            continue;
        }
        let name = outcome.name.replace('_', " ");
        let line = format!(
            "{name}|{{{{POTY{year}/votebutton|f={name}|base=Commons:Picture_of_the_Year/{year}/R2}}}} <!-- #{} overall, #{} in {} -->",
            outcome.overall_rank, outcome.cat_rank, outcome.category
        );
        text.push(line);
    }
    // Shuffle the images by default
    let mut rng = thread_rng();
    text.shuffle(&mut rng);
    let gallery = text.join("\n");
    format!("<gallery widths=400 heights=400>\n{gallery}\n</gallery>")
}

fn format_gallery2(outcomes: &[R2Outcome], year: usize) -> String {
    let mut text = vec![];
    for outcome in outcomes {
        let name = outcome.name.replace('_', " ");
        let line = format!(
            "{name}|{{{{POTY/VoteConstructor|rank={}|votes={}}}}}",
            outcome.overall_rank, outcome.counted_votes
        );
        text.push(line);
    }
    let count = outcomes.len();
    let gallery = text.join("\n");
    format!(
        r#"{{{{POTY/header|{year}}}}}{{{{POTY/header/navi|{year}}}}}{{{{POTYx/langnav18n|3=<languages />}}}}__NOTOC__
<center><h1><translate>Final results (all {count})</translate></h1></center>
<gallery class="center" mode="nolines" widths="233" heights="185" showfilename>
{gallery}
</gallery>"#
    )
}

pub(crate) fn tag_images(
    user: UserGuard,
    outcomes: Vec<R2Outcome>,
    sender: &ConsoleSender,
    year: usize,
) {
    let sender = sender.clone();
    tokio::spawn(async move {
        let bot = user.bot().await?;
        let opts = SaveOptions::summary("POTY results");
        for outcome in outcomes {
            let page = bot.page(&format!("File:{}", outcome.name))?;
            let html = page.html().await?;
            let html = update_assessment(html, &outcome, year)?;
            page.save(html, &opts).await?;
            let _ = sender.send(
                user.console(&format!("Saved [[File:{}]]", outcome.name)),
            );
        }
        anyhow::Ok(())
    });
}

fn update_assessment(
    html: ImmutableWikicode,
    outcome: &R2Outcome,
    year: usize,
) -> Result<ImmutableWikicode> {
    let html = html.into_mutable();
    for temp in html.filter_templates()? {
        if temp.name() == "Template:Assessments" {
            temp.set_param(
                "POTY",
                match outcome.overall_rank {
                    1..=3 => outcome.overall_rank.to_string(),
                    _ => "f".to_string(),
                }
                .as_str(),
            )?;
            temp.set_param("POTYyear", &year.to_string())?;
        }
    }
    Ok(html.into_immutable())
}

pub(crate) async fn publish_results(
    guard: UserGuard,
    outcomes: &[R1Outcome],
    logger: &ConsoleSender,
    year: usize,
    account_eligibility: usize,
) -> Result<()> {
    let wikitext = format_wikitext(outcomes, year, account_eligibility);
    let bot = guard.bot().await?;
    let page =
        bot.page(&format!("Commons:Picture of the Year/{year}/R1/Results"))?;
    let _ = logger.send(guard.console(&format!("Saving [[{}]]", page.title())));
    page.save(
        wikitext,
        &SaveOptions::summary("Publishing Round 1 results"),
    )
    .await?;
    let gallery = format_gallery(outcomes, year);
    let page2 =
        bot.page(&format!("Commons:Picture of the Year/{year}/Candidates/R2"))?;
    let _ =
        logger.send(guard.console(&format!("Saving [[{}]]", page2.title())));
    page2
        .save(gallery, &SaveOptions::summary("Publishing Round 1 results"))
        .await?;
    Ok(())
}

pub(crate) async fn publish_results2(
    guard: UserGuard,
    outcomes: &[R2Outcome],
    logger: &ConsoleSender,
    year: usize,
    account_eligibility: usize,
) -> Result<()> {
    let wikitext = format_wikitext2(outcomes, year, account_eligibility);
    let bot = guard.bot().await?;
    let page =
        bot.page(&format!("Commons:Picture of the Year/{year}/Results/Raw"))?;
    let _ = logger.send(guard.console(&format!("Saving [[{}]]", page.title())));
    page.save(
        wikitext,
        &SaveOptions::summary("Publishing Round 2 results"),
    )
    .await?;
    let gallery = format_gallery2(outcomes, year);
    let page2 =
        bot.page(&format!("Commons:Picture of the Year/{year}/Results/All"))?;
    let _ =
        logger.send(guard.console(&format!("Saving [[{}]]", page2.title())));
    page2
        .save(gallery, &SaveOptions::summary("Publishing Round 2 results"))
        .await?;
    Ok(())
}

fn parse_candidates(
    html: ImmutableWikicode,
    bot: &Bot,
) -> HashMap<String, Vec<R1Outcome>> {
    let html = html.into_mutable();
    let mut result = HashMap::new();
    for section in html.iter_sections() {
        let heading = match section.heading() {
            Some(heading) => heading,
            None => {
                continue;
            }
        };
        let category = heading.text_contents();
        let images: Vec<_> = section
            .descendants()
            .filter_map(|node| node.as_gallery())
            .flat_map(|gallery| gallery.images())
            .map(|image| {
                let page =
                    bot.page(&image.title()).expect("invalid page title");
                let dbkey = page.as_title().dbkey().to_string();
                R1Outcome {
                    name: dbkey,
                    category: category.to_string(),
                    total_votes: 0,
                    counted_votes: 0,
                    ineligible: Default::default(),
                    overall_rank: usize::MAX,
                    cat_rank: usize::MAX,
                    advances: false,
                }
            })
            .collect();
        result.insert(category, images);
    }
    result
}

#[derive(Serialize)]
pub(crate) struct R2Outcome {
    name: String,
    total_votes: usize,
    counted_votes: usize,
    ineligible: BTreeSet<String>,
    overall_rank: usize,
}

pub(crate) async fn tally_r2(
    commonsdb: &CommonsDB,
    toolsdb: &ToolsDB,
    user: &UserGuard,
    logger: &ConsoleSender,
    year: usize,
) -> Result<Vec<R2Outcome>> {
    let bot = user.bot().await?;
    let page =
        bot.page(&format!("Commons:Picture of the Year/{year}/Candidates/R2"))?;
    let html = page.html().await?;
    let mut outcomes: Vec<_> = {
        let html = html.into_mutable();
        html.inclusive_descendants()
            .filter_map(|node| node.as_gallery())
            .flat_map(|gallery| gallery.images())
            .map(|image| {
                let page =
                    bot.page(&image.title()).expect("invalid page title");
                let dbkey = page.as_title().dbkey().to_string();
                R2Outcome {
                    name: dbkey,
                    total_votes: 0,
                    counted_votes: 0,
                    ineligible: Default::default(),
                    overall_rank: 0,
                }
            })
            .collect()
    };
    let mut votes = query_votes(commonsdb, year, 2).await?;
    let ineligible =
        crate::eligibility::ineligible_voters(toolsdb, year).await?;
    // Step 1: Assign vote counts, remove ineligible voters
    for outcome in outcomes.iter_mut() {
        if let Some(voters) = votes.remove(&outcome.name) {
            outcome.total_votes = voters.len();
            // Counted votes is total minus ineligible
            outcome.counted_votes = voters.difference(&ineligible).count();
            // Keep track of the ineligible voters who were ignored
            outcome.ineligible =
                voters.intersection(&ineligible).cloned().collect();
        }
    }
    if !votes.is_empty() {
        let names = votes.into_keys().collect::<Vec<_>>().join(", ");
        let _ =
            logger.send(user.console(&format!(
                "Error: some votes were not tallied: {names}"
            )));
    }
    // Step 3: Assign overall ranks
    let values: HashSet<usize> = outcomes
        .iter()
        .map(|outcome| outcome.counted_votes)
        .collect();
    let mut values: Vec<_> = values.into_iter().collect();
    values.sort();
    values.reverse();
    let mut rank: usize = 1;
    for value in values {
        let mut offset = 0;
        for outcome in &mut outcomes {
            if outcome.counted_votes == value {
                // Assign rank
                outcome.overall_rank = rank;
                offset += 1;
            }
        }
        rank += offset;
    }
    // Step 4: Sort by total votes
    outcomes.sort_by_key(|outcome| outcome.counted_votes);
    outcomes.reverse();
    Ok(outcomes)
}

async fn query_votes(
    commonsdb: &CommonsDB,
    year: usize,
    round: usize,
) -> Result<HashMap<String, HashSet<String>>> {
    let mut conn = commonsdb.0.get_conn().await?;
    if round > 2 {
        panic!("invalid round: {round}");
    }
    let title = format!("Picture_of_the_Year/{year}/R{round}/v/%");
    let mut map: HashMap<String, HashSet<String>> = HashMap::new();
    let rows: Vec<(String, String)> = conn
        .exec(
            r#"
select lt_title, page_title
from pagelinks
join linktarget on pl_target_id=lt_id
join page on pl_from=page_id
where page_namespace=4
and page_title like ?
and lt_namespace=2
and pl_from_namespace=4
"#,
            (title,),
        )
        .await?;
    for (voter, page) in rows {
        // unwrap: SQL verifies page has a slash in it
        let (_, title) = page.rsplit_once('/').unwrap();
        map.entry(title.to_string()).or_default().insert(voter);
    }
    Ok(map)
}
