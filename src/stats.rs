use crate::CommonsDB;
use anyhow::Result;
use mysql_async::prelude::Queryable;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, BTreeSet, HashSet};

pub(crate) async fn query_votecount(
    pool: &CommonsDB,
    round: usize,
    year: usize,
    ineligible: &HashSet<String>,
) -> Result<(usize, usize)> {
    let mut conn = pool.0.get_conn().await?;
    let title = format!("Picture_of_the_Year/{year}/R{round}/v/%");
    let votes: usize = conn
        .exec_first(
            r#"
select COUNT(pl_target_id)
from pagelinks
join linktarget on pl_target_id=lt_id
join page on pl_from=page_id
where page_namespace=4
and page_title like ?
and lt_namespace=2
and pl_from_namespace=4;
"#,
            (&title,),
        )
        .await?
        .unwrap();
    let voters: HashSet<String> = conn
        .exec(
            r#"
select DISTINCT lt_title
from pagelinks
join linktarget on pl_target_id=lt_id
join page on pl_from=page_id
where page_namespace=4
and page_title like ?
and lt_namespace=2
and pl_from_namespace=4;
"#,
            (&title,),
        )
        .await?
        .into_iter()
        .collect();
    let eligible_voters = voters.difference(ineligible).count();
    Ok((votes, eligible_voters))
}

pub(crate) async fn query_blocked(
    pool: &CommonsDB,
    year: usize,
) -> Result<BTreeSet<String>> {
    let mut conn = pool.0.get_conn().await?;
    let title = format!("Picture_of_the_Year/{year}/R_/v/%");
    let result = conn
        .exec(
            r#"
select DISTINCT lt_title
from pagelinks
join linktarget on pl_target_id=lt_id
join page on pl_from=page_id
join user on REPLACE(lt_title, '_', ' ')=user_name
left join ipblocks on user_id=ipb_user
where page_namespace=4
and page_title like ?
and lt_namespace=2
and pl_from_namespace=4
and ipb_id is not null
"#,
            (title,),
        )
        .await?
        .into_iter()
        .collect();
    Ok(result)
}

/// Find R2 voters who cast more than 3 votes
pub(crate) async fn query_extra_r2_votes(
    pool: &CommonsDB,
    year: usize,
) -> Result<BTreeMap<String, usize>> {
    let mut conn = pool.0.get_conn().await?;
    let title = format!("Picture_of_the_Year/{year}/R2/v/%");
    let result: Vec<(String, usize)> = conn
        .exec(
            r#"
select
  lt_title,
  COUNT(*)
from
  pagelinks
  join linktarget on pl_target_id = lt_id
  join page on pl_from = page_id
where
  page_namespace = 4
  and page_title like ?
  and lt_namespace = 2
  and pl_from_namespace = 4
group by
  lt_title
having
  count(lt_title) > 3
"#,
            (title,),
        )
        .await?;
    Ok(result.into_iter().collect())
}

#[derive(Serialize)]
pub(crate) struct RenamedUser {
    old_name: String,
    new_name: String,
    log_id: usize,
}

pub(crate) async fn query_renamed(
    pool: &CommonsDB,
    year: usize,
) -> Result<Vec<RenamedUser>> {
    let mut conn = pool.0.get_conn().await?;
    let title = format!("Picture_of_the_Year/{year}/R_/v/%");
    // TODO: narrow this down to the POTY timeframe
    let timestamp_like = format!("{}%", year + 1);
    let result = conn
        .exec_map(
            r#"
select
    DISTINCT lt_title,
    log_id,
    log_params
from
    pagelinks
    join linktarget on pl_target_id = lt_id
    join page on pl_from = page_id
    join logging_logindex on lt_title = log_title
where
    page_namespace = 4
    and page_title like ?
    and lt_namespace = 2
    and pl_from_namespace = 4
    and log_namespace = 2
    and log_type = "renameuser"
    and log_action = "renameuser"
    and log_timestamp like ?
    order by log_timestamp desc
"#,
            (title, timestamp_like),
            |(lt_title, log_id, log_params): (String, usize, String)| {
                let new_name = php_serde::from_bytes::<RenameuserParams>(
                    log_params.as_bytes(),
                )
                .map(|params| params.newuser)
                .unwrap_or_else(|err| {
                    eprintln!("error parsing log_params (#{log_id}): {err}");
                    "Invalid log_params in database".to_string()
                });
                RenamedUser {
                    old_name: lt_title,
                    new_name,
                    log_id,
                }
            },
        )
        .await?;
    Ok(result)
}

#[derive(Deserialize, Debug)]
struct RenameuserParams {
    #[serde(rename = "4::olduser")]
    _olduser: String,
    #[serde(rename = "5::newuser")]
    newuser: String,
}

#[test]
fn test_renameuser_params() {
    let params = r#"a:3:{s:10:"4::olduser";s:10:"EpicPupper";s:10:"5::newuser";s:7:"Frostly";s:8:"6::edits";i:2141;}"#;
    let value: RenameuserParams =
        php_serde::from_bytes(params.as_bytes()).unwrap();
    dbg!(&value);
    assert_eq!(&value._olduser, "EpicPupper");
    assert_eq!(&value.newuser, "Frostly");
}
