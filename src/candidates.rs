use crate::guard::UserGuard;
use crate::ConsoleSender;
use anyhow::{anyhow, Result};
use mwbot::parsoid::{ImmutableWikicode, Wikicode, WikinodeIterator};
use mwbot::{Bot, Page, SaveOptions};
use rocket::form::validate::Contains;
use std::collections::{BTreeMap, HashMap, HashSet};
use tracing::{debug, error, info};

type CategorizedCandidates = BTreeMap<String, Vec<Picture>>;
const MONTHS: [&str; 12] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];

fn is_featured(code: &Wikicode) -> bool {
    code.inclusive_descendants()
        .filter_map(|node| node.as_indicator())
        .any(|indicator| indicator.name().unwrap() == "Cscr-featured.svg")
}

fn find_nom_link(code: &Wikicode) -> Result<String> {
    for link in code.filter_links() {
        let target = link.target();
        if target.starts_with("Commons:Featured picture candidates/") {
            return Ok(target);
        }
    }
    todo!("I accidentally deleted this code")
}

async fn find_removal_gallery(
    bot: &Bot,
    nom_page: Page,
    nom_html: ImmutableWikicode,
) -> Result<Option<String>> {
    let target = {
        let mut ret = None;
        let html = nom_html.into_mutable();
        for temp in html.filter_templates()? {
            if temp.name() == "Template:FPC-delist-results-reviewed" {
                for link in html.filter_links() {
                    let target = link.target();
                    if target
                        .starts_with("Commons:Featured picture candidates/")
                        && link.text_contents() == "Original nomination"
                    {
                        ret = Some(target);
                        break;
                    }
                }
            }
        }
        ret
    };
    match target {
        Some(target) => {
            info!("Found a removal!: {target}");
            let page = bot.page(&target)?;
            find_gallery(&target, page.html().await?)
        }
        None => Err(anyhow!(
            "{} was delisted, can't find original nomination",
            nom_page.title()
        )),
    }
}

/// Strip whitespace and replace underscores with spaces
fn normalize_gallery(input: &str) -> String {
    input.trim().replace('_', " ")
}

fn find_gallery(
    title: &str,
    html: ImmutableWikicode,
) -> Result<Option<String>> {
    let html = html.into_mutable();
    for temp in html.filter_templates()? {
        if temp.name() == "Template:FPC-results-reviewed" {
            let gallery = temp
                .param("gallery")
                .or_else(|| temp.param("category"))
                .ok_or_else(|| {
                    anyhow!(
                        "{title}: Unable to find gallery/category parameter"
                    )
                })?;
            let gallery = if gallery.contains('#') {
                gallery.split_once('#').unwrap().0
            } else {
                gallery.trim()
            };
            return Ok(Some(normalize_gallery(gallery)));
        }
    }
    error!(
        "Couldn't find template on {}",
        html.title().unwrap_or("unknown title".to_string())
    );
    Ok(None)
}

/// Follow redirects using just HTML
async fn follow_redirect(
    bot: &Bot,
    page: Page,
) -> Result<(Page, ImmutableWikicode)> {
    let html = page.html().await?;
    let target = {
        let html = html.clone().into_mutable();
        html.redirect().map(|redirect| redirect.target())
    };
    match target {
        Some(title) => {
            let page = bot.page(&title)?;
            let html = page.html().await?;
            Ok((page, html))
        }
        None => Ok((page, html)),
    }
}

async fn handle_image(
    bot: &Bot,
    image_page: Page,
    fp_id: String,
) -> Result<Option<Picture>> {
    // Follow any redirect
    let (image_page, image_html) = follow_redirect(bot, image_page).await?;
    let nom_link = {
        let html = image_html.into_mutable();
        if !is_featured(&html) {
            error!("{} is not featured!!", image_page.title());
            return Ok(None);
        }
        find_nom_link(&html)?
    };
    let nom_page = bot.page(&nom_link)?;
    let (nom_page, nom_html) = follow_redirect(bot, nom_page).await?;
    debug!("Parsing {}...", nom_page.title());
    let gallery = if nom_page.title().contains("/removal/") {
        find_removal_gallery(bot, nom_page, nom_html).await?
    } else {
        find_gallery(nom_page.title(), nom_html)?
    }
    .unwrap_or_default();
    debug!("gallery={}", &gallery);
    let picture = Picture {
        title: image_page.title().to_string(),
        category: poty_category(&gallery),
        gallery,
        fp_id,
    };
    info!("Checked {}...", image_page.title());
    Ok(Some(picture))
}

fn poty_category(gallery: &str) -> Option<String> {
    let categories: HashSet<&'static str> =
        poty_categories().into_keys().collect();
    if gallery.contains("Interiors/Religious buildings") {
        return Some("Interiors of religious buildings".to_string());
    } else if gallery.contains("Architecture/Religious buildings") {
        return Some("Religious Buildings".to_string());
    }
    for part in gallery.split('/').rev() {
        if categories.contains(part) {
            return Some(part.to_string());
        }
        let mapping = HashMap::from([
            ("Animals", "Other animals"),
            ("Shells", "Objects, shells and miscellaneous"),
            ("Objects", "Objects, shells and miscellaneous"),
            ("Vehicles", "Vehicles and crafts"),
            ("Castles and fortifications", "Castles"),
            ("Food and drink", "Objects, shells and miscellaneous"),
            ("Fungi", "Plants and fungi"),
            ("Plants", "Plants and fungi"),
            ("Maps", "Paintings, textiles and works on paper"),
            ("Satellite images", "Astronomy"),
            ("Space exploration", "Astronomy"),
            ("Sports", "People"),
            ("Interiors", "Interiors and details"),
            ("Bridges", "Constructions and buildings"),
            ("Towers", "Constructions and buildings"),
            (
                "Non-photographic media",
                "Paintings, textiles and works on paper",
            ),
        ]);
        if let Some(cat) = mapping.get(part) {
            return Some(cat.to_string());
        }
    }
    None
}

fn poty_categories() -> HashMap<&'static str, &'static str> {
    HashMap::from([
        ("Arthropods", "Arthropods"),
        ("Birds", "Birds"),
        ("Mammals", "Mammals"),
        ("Other animals", "Other animals"),
        ("Plants and fungi", "Plants and fungi"),
        ("People", "People and human activities"),
        (
            "Paintings, textiles and works on paper",
            "Paintings, textiles and works on paper",
        ),
        ("Settlements", "Settlements"),
        ("Castles", "Castles and Fortifications"),
        ("Religious Buildings", "Religious Buildings"),
        ("Constructions and buildings", "Constructions and buildings"),
        (
            "Artificially illuminated outdoor spaces",
            "Artificially illuminated outdoor spaces",
        ),
        ("Infrastructure", "Infrastructure"),
        ("Interiors and details", "Interiors and details"),
        (
            "Interiors of religious buildings",
            "Interiors of religious buildings",
        ),
        (
            "Frescos, ceilings and stained glass",
            "Frescos, ceilings and stained glass",
        ),
        ("Panoramic views", "Panoramic views"),
        ("Nature views", "Nature views"),
        ("Waters", "Waters"),
        ("Astronomy", "Astronomy, satellite and outer space"),
        ("Vehicles and crafts", "Vehicles and crafts"),
        ("Sculptures", "Sculptures"),
        (
            "Objects, shells and miscellaneous",
            "Objects, shells and miscellaneous",
        ),
    ])
}

#[derive(Debug, Clone)]
pub(crate) struct Picture {
    title: String,
    gallery: String,
    category: Option<String>,
    fp_id: String,
}

pub(crate) async fn fetch_candidates(
    user: &UserGuard,
    logger: &ConsoleSender,
    year: usize,
) -> Result<(CategorizedCandidates, Vec<String>)> {
    let _ = logger.send(user.console("Fetching candidates..."));
    let bot = user.bot().await?;
    let mut handles = vec![];
    for month in 1u32..=12 {
        let mut counter: usize = 0;
        let page = bot.page(&format!(
            "Commons:Featured pictures/chronological/{} {}",
            MONTHS[(month - 1) as usize],
            year
        ))?;
        info!("Extracting from {}...", page.title());
        let html = page.html().await?.into_mutable();
        let galleries: Vec<_> = html
            .inclusive_descendants()
            .filter_map(|node| node.as_gallery())
            .collect();
        for gallery in galleries {
            for image in gallery.images() {
                if [
                    "File:Symbol support vote.svg",
                    "File:Symbol oppose vote.svg",
                    "File:Symbol neutral vote.svg",
                ]
                .contains(&image.title().as_str())
                {
                    continue;
                }
                counter += 1;
                let fp_id = format!("{year}-{month}/{counter}");
                let image_page = bot.page(&image.title())?;
                let bot = bot.clone();
                handles.push(tokio::spawn(async move {
                    handle_image(&bot, image_page, fp_id).await
                }));
            }
        }
    }
    let mut categorized: CategorizedCandidates = BTreeMap::new();
    let mut errors = vec![];
    for handle in handles {
        match handle.await? {
            Ok(Some(picture)) => {
                let category =
                    picture.category.clone().unwrap_or("dummy".to_string());
                categorized
                    .entry(category)
                    .and_modify(|v| v.push(picture.clone()))
                    .or_insert_with(|| vec![picture]);
            }
            Ok(None) => {}
            Err(err) => errors.push(err.to_string()),
        }
    }
    // Make sure all categories are included, even if they're empty. Humans
    // can consolidate later.
    for cat in poty_categories().keys() {
        categorized.entry(cat.to_string()).or_default();
    }
    let _ = logger.send(user.console("Finished fetching candidates!"));
    Ok((categorized, errors))
}

pub(crate) async fn save_candidates(
    user: &UserGuard,
    categorized: CategorizedCandidates,
    logger: &ConsoleSender,
    year: usize,
) -> Result<()> {
    let bot = user.bot().await?;
    let all_categories = poty_categories();
    let mut text = vec![r#"
{{autotranslate|base=POTY/header|%%YEAR%%}}{{autotranslate|base=POTY/header/navi|%%YEAR%%}}
{| cellspacing="0" cellpadding="0" style="clear:right; margin-bottom: .5em; float: right; padding: .5em 0 .8em 1.4em; background: none;"
| __TOC__
|}
"#.replace("%%YEAR%%", year.to_string().as_str())];
    for (category, pictures) in categorized {
        let mut section = vec![format!(
            "== [[Commons:Picture of the Year/{year}/R1/Gallery/{category}|{}]] ==",
            all_categories.get(&category.as_str()).map(|c| c.to_string()).unwrap_or_else(||category.to_string())
        ),
        "<gallery widths=300 heights=300 mode=nolines>".to_string()];
        for picture in pictures {
            let name = picture.title.strip_prefix("File:").unwrap();
            section.push(format!(
                "{name}|{} <!-- {} -->",
                picture.fp_id, picture.gallery
            ))
        }
        section.push("</gallery>".to_string());
        text.push(section.join("\n"));
    }
    let text = text.join("\n");
    let title = format!("Commons:Picture of the Year/{year}/Candidates");
    let page = bot.page(&title)?;
    page.save(
        text,
        &SaveOptions::summary(&format!("Initializing POTY {year}")),
    )
    .await?;
    let _ = logger.send(user.console(&format!("Saved [[{title}]]")));
    Ok(())
}
