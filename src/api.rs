// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use mwapi::Client;

pub async fn user_info(client: &Client) -> Result<String> {
    let resp = client
        .get_value(&[("action", "query"), ("meta", "userinfo")])
        .await?;
    Ok(resp["query"]["userinfo"]["name"]
        .as_str()
        .unwrap()
        .to_string())
}
