// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use anyhow::anyhow;
use cached::proc_macro::once;
use checks::ImageResolution;
use config::committee;
use mwbot::Bot;
use mysql_async::Pool;
use rocket::fairing::AdHoc;
use rocket::fs::FileServer;
use rocket::http::{Cookie, CookieJar, SameSite, Status};
use rocket::response::{
    stream::{Event, EventStream},
    Redirect,
};
use rocket::serde::json::Json;
use rocket::{Request, State};
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use rocket_oauth2::{OAuth2, TokenResponse};
use serde::Serialize;
use std::collections::{BTreeMap, BTreeSet};
use time::OffsetDateTime;
use tokio::sync::broadcast::{self, error::RecvError};
use tokio::time::Duration;

mod api;
mod candidates;
mod catstats;
mod checks;
mod config;
mod eligibility;
mod guard;
mod init;
mod spamlist;
mod stats;
mod tally;
mod uploaders;
mod uservotes;
mod votepages;

use crate::config::{current_year, poty_state, POTYState};
use crate::stats::RenamedUser;
use crate::tally::{R1Outcome, R2Outcome};
use catstats::CatStats;
use guard::UserGuard;

type ConsoleSender = broadcast::Sender<ConsoleEvent>;

const USER_AGENT: &str = toolforge::user_agent!("poty-stuff");
const COOKIE_EXPIRY: u64 = 60 * 60; // 1 hour

#[derive(Clone)]
struct CommonsDB(Pool);
#[derive(Clone)]
struct ToolsDB(Pool);

#[derive(Serialize)]
struct ErrorTemplate {
    code: u16,
    reason: &'static str,
    error: Option<String>,
}

#[derive(Responder)]
#[response(status = 500, content_type = "text/html")]
struct Error(Template);

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        Self(Template::render(
            "error",
            ErrorTemplate {
                code: 500,
                reason: "Internal Server Error",
                error: Some(value.to_string()),
            },
        ))
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[once(sync_writes = true, result = true)]
async fn commons_bot() -> anyhow::Result<Bot> {
    Ok(Bot::builder(
        "https://commons.wikimedia.org/w/api.php".to_string(),
        "https://commons.wikimedia.org/api/rest_v1".to_string(),
    )
    .set_user_agent(USER_AGENT.to_string())
    .build()
    .await?)
}

#[derive(Serialize)]
struct IndexTemplate {
    username: Option<String>,
    committee: Vec<String>,
    year: usize,
}

#[get("/")]
async fn index(user: Option<UserGuard>) -> Template {
    // We intentionally don't fail here so we at least have a landing page.
    let (state, committee) = tokio::join!(poty_state(), committee());
    let (year, _) = current_year(state.unwrap_or_default());
    let committee = committee.unwrap_or_default();
    Template::render(
        "index",
        IndexTemplate {
            username: user.map(|g| g.username),
            committee,
            year,
        },
    )
}

#[derive(Debug)]
struct MWOAuth;

#[get("/login")]
fn login(oauth2: OAuth2<MWOAuth>, cookies: &CookieJar<'_>) -> Redirect {
    oauth2.get_redirect(cookies, &[]).unwrap()
}

#[get("/auth")]
async fn auth(
    token: TokenResponse<MWOAuth>,
    cookies: &CookieJar<'_>,
) -> Result<Redirect, Status> {
    let client =
        mwapi::Client::builder("https://commons.wikimedia.org/w/api.php")
            .set_oauth2_token(token.access_token())
            .set_user_agent(USER_AGENT)
            .build()
            .await
            .unwrap();
    let username = api::user_info(&client).await.unwrap();
    let commitee = committee().await.unwrap();
    if !commitee.contains(&username) {
        // Unauthorized
        return Err(Status::Unauthorized);
    }
    // Finally, set a login cookie
    cookies.add_private(
        Cookie::build((
            "username",
            serde_json::to_string(&UserGuard {
                username,
                oauth2_token: token.access_token().to_string(),
            })
            .expect("failed to encode JSON"),
        ))
        .same_site(SameSite::Lax)
        .expires(
            OffsetDateTime::now_utc() + Duration::from_secs(COOKIE_EXPIRY),
        ),
    );
    Ok(Redirect::to("/"))
}

#[derive(Serialize)]
struct InitTemplate {
    username: String,
    pages: Vec<(String, String, bool)>,
}

#[get("/init")]
async fn init_get(
    user: UserGuard,
    sender: &State<ConsoleSender>,
) -> Result<Template> {
    let (year, _) = current_year(poty_state().await?);
    let tokens = init::Tokens::new(year);
    let pages = init::fetch(&user, &tokens, sender).await?;
    Ok(Template::render(
        "init",
        InitTemplate {
            username: user.username,
            pages,
        },
    ))
}

#[post("/init/create")]
async fn init_create(
    user: UserGuard,
    sender: &State<ConsoleSender>,
) -> Result<Json<bool>> {
    let (year, _) = current_year(poty_state().await?);
    let tokens = init::Tokens::new(year);
    let pages = init::fetch(&user, &tokens, sender).await?;
    init::create(&user, &tokens, sender, pages).await?;
    Ok(Json(true))
}

#[derive(Serialize)]
struct CandidatesTemplate {
    username: String,
    candidates: BTreeMap<String, usize>,
    errors: Vec<String>,
    total: usize,
}

#[get("/candidates")]
async fn candidates_get(
    user: UserGuard,
    sender: &State<ConsoleSender>,
) -> Result<Template> {
    let (year, _) = current_year(poty_state().await?);
    let (candidates, errors) =
        candidates::fetch_candidates(&user, sender, year).await?;
    let candidates: BTreeMap<_, _> =
        candidates.into_iter().map(|(k, v)| (k, v.len())).collect();
    let total = candidates.values().sum();
    Ok(Template::render(
        "candidates",
        CandidatesTemplate {
            username: user.username,
            candidates,
            errors,
            total,
        },
    ))
}

#[post("/candidates/create")]
async fn candidates_create(
    user: UserGuard,
    sender: &State<ConsoleSender>,
) -> Result<Json<bool>> {
    let (year, _) = current_year(poty_state().await?);
    let (categorized, errors) =
        candidates::fetch_candidates(&user, sender, year).await?;
    if !errors.is_empty() {
        Err(anyhow!("there are errors, cannot save"))?
    }
    candidates::save_candidates(&user, categorized, sender, year).await?;
    Ok(Json(true))
}

#[derive(Serialize)]
struct CatStatsTemplate {
    username: Option<String>,
    stats: CatStats,
}

#[get("/catstats/<year>")]
async fn catstats_get_year(
    user: Option<UserGuard>,
    year: usize,
) -> Result<Template> {
    let stats = catstats::catstats(year).await?;
    Ok(Template::render(
        "catstats",
        CatStatsTemplate {
            username: user.map(|u| u.username),
            stats,
        },
    ))
}

#[get("/catstats")]
async fn catstats_get() -> Result<Redirect> {
    let (year, _) = current_year(poty_state().await?);
    Ok(Redirect::temporary(uri!(catstats_get_year(year))))
}

#[derive(Serialize)]
struct ChecksTemplate {
    username: Option<String>,
    dupes: Vec<String>,
    formerly_featured: Vec<String>,
    pics_by_ratio: Vec<(String, ImageResolution)>,
}

#[get("/checks/<year>")]
async fn checks_get_year(
    user: Option<UserGuard>,
    pool: &State<CommonsDB>,
    year: usize,
) -> Result<Template> {
    let (dupes, formerly_featured, pics_by_ratio) = tokio::try_join!(
        checks::duplicates(year),
        checks::formerly_featured(pool, year),
        checks::pics_by_ratio(year, pool),
    )?;
    Ok(Template::render(
        "checks",
        ChecksTemplate {
            username: user.map(|u| u.username),
            dupes,
            formerly_featured,
            pics_by_ratio,
        },
    ))
}

#[get("/checks")]
async fn checks_get() -> Result<Redirect> {
    let (year, _) = current_year(poty_state().await?);
    Ok(Redirect::temporary(uri!(checks_get_year(year))))
}

#[derive(Serialize)]
struct VotePagesTemplate {
    username: String,
    existing_1: usize,
    existing_2: usize,
}

#[get("/votepages")]
async fn votepages_get(
    user: UserGuard,
    pool: &State<CommonsDB>,
) -> Result<Template> {
    let (year, _) = current_year(poty_state().await?);
    let (existing_1, existing_2) = tokio::try_join!(
        votepages::existing(pool, year, 1),
        votepages::existing(pool, year, 2),
    )?;
    Ok(Template::render(
        "votepages",
        VotePagesTemplate {
            username: user.username,
            existing_1: existing_1.len(),
            existing_2: existing_2.len(),
        },
    ))
}

#[post("/votepages/<round>")]
async fn votepages_post(
    user: UserGuard,
    round: usize,
    pool: &State<CommonsDB>,
    sender: &State<ConsoleSender>,
) -> Result<Json<bool>> {
    let (year, _) = current_year(poty_state().await?);
    let existing = votepages::existing(pool, year, round).await?;
    votepages::create(user, sender, year, round, existing);
    Ok(Json(true))
}

#[derive(Serialize)]
struct TallyR2Template {
    username: String,
    year: usize,
    accounteligibility: usize,
    outcomes: Vec<R2Outcome>,
    wikitext: String,
}

#[get("/tally_r2")]
async fn tally_r2_get(
    user: UserGuard,
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
    sender: &State<ConsoleSender>,
) -> Result<Template> {
    let (year, cfg) = current_year(poty_state().await?);
    let outcomes =
        tally::tally_r2(commonsdb, toolsdb, &user, sender, year).await?;
    let wikitext = tally::format_wikitext2(&outcomes, year, cfg.eligibility.id);
    Ok(Template::render(
        "tally_r2",
        TallyR2Template {
            username: user.username,
            year,
            accounteligibility: cfg.eligibility.id,
            outcomes,
            wikitext,
        },
    ))
}

#[post("/tally_r2/publish")]
async fn tally_r2_post(
    user: UserGuard,
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
    sender: &State<ConsoleSender>,
) -> Result<Json<bool>> {
    let (year, cfg) = current_year(poty_state().await?);
    let outcomes =
        tally::tally_r2(commonsdb, toolsdb, &user, sender, year).await?;
    tally::publish_results2(user, &outcomes, sender, year, cfg.eligibility.id)
        .await?;
    Ok(Json(true))
}

#[post("/tally_r2/images")]
async fn tally_r2_images(
    user: UserGuard,
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
    sender: &State<ConsoleSender>,
) -> Result<Json<bool>> {
    let (year, _) = current_year(poty_state().await?);
    let outcomes =
        tally::tally_r2(commonsdb, toolsdb, &user, sender, year).await?;
    tally::tag_images(user, outcomes, sender, year);
    Ok(Json(true))
}

#[derive(Serialize)]
struct TallyR1Template {
    username: String,
    year: usize,
    accounteligibility: usize,
    outcomes: Vec<R1Outcome>,
    wikitext: String,
}

#[get("/tally_r1")]
async fn tally_r1_get(
    user: UserGuard,
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
    sender: &State<ConsoleSender>,
) -> Result<Template> {
    let (year, cfg) = current_year(poty_state().await?);
    let outcomes =
        tally::tally_r1(commonsdb, toolsdb, &user, sender, year).await?;
    let wikitext = tally::format_wikitext(&outcomes, year, cfg.eligibility.id);
    Ok(Template::render(
        "tally_r1",
        TallyR1Template {
            username: user.username,
            year,
            accounteligibility: cfg.eligibility.id,
            outcomes,
            wikitext,
        },
    ))
}

#[post("/tally_r1/publish")]
async fn tally_r1_post(
    user: UserGuard,
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
    sender: &State<ConsoleSender>,
) -> Result<Json<bool>> {
    let (year, cfg) = current_year(poty_state().await?);
    let outcomes =
        tally::tally_r1(commonsdb, toolsdb, &user, sender, year).await?;
    tally::publish_results(user, &outcomes, sender, year, cfg.eligibility.id)
        .await?;
    Ok(Json(true))
}

#[derive(Serialize)]
struct UserVotesTemplate {
    username: Option<String>,
    thisuser: String,
    year: usize,
    r1_votes: BTreeSet<String>,
    r2_votes: BTreeSet<String>,
}

#[get("/uservotes?<username>&<year>")]
async fn uservotes_get(
    username: String,
    year: usize,
    guard: Option<UserGuard>,
    pool: &State<CommonsDB>,
) -> Result<Template> {
    let (r1_votes, r2_votes) =
        uservotes::query_votes(pool, year, &username).await?;
    Ok(Template::render(
        "uservotes",
        UserVotesTemplate {
            username: guard.map(|u| u.username),
            thisuser: username,
            r1_votes,
            r2_votes,
            year,
        },
    ))
}

#[derive(Serialize)]
struct StatsTemplate {
    username: Option<String>,
    year: usize,
    votes1: usize,
    voters1: usize,
    votes2: usize,
    voters2: usize,
    ineligible_voters: BTreeSet<String>,
    eligible_voters: usize,
    blocked_voters: BTreeSet<String>,
    renamed: Vec<RenamedUser>,
    extra_r2_votes: BTreeMap<String, usize>,
}

#[get("/stats")]
async fn stats_get() -> Result<Redirect> {
    let (year, _) = current_year(poty_state().await?);
    Ok(Redirect::temporary(uri!(stats_year_get(year))))
}

#[get("/stats/<year>")]
async fn stats_year_get(
    year: usize,
    user: Option<UserGuard>,
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
) -> Result<Template> {
    let ineligible_voters =
        eligibility::ineligible_voters(toolsdb, year).await?;
    let (
        (votes1, voters1),
        (votes2, voters2),
        eligible_voters,
        blocked_voters,
        renamed,
        extra_r2_votes,
    ) = tokio::try_join!(
        stats::query_votecount(commonsdb, 1, year, &ineligible_voters),
        stats::query_votecount(commonsdb, 2, year, &ineligible_voters),
        eligibility::eligible_voters_count(toolsdb, year),
        stats::query_blocked(commonsdb, year),
        stats::query_renamed(commonsdb, year),
        stats::query_extra_r2_votes(commonsdb, year)
    )?;
    Ok(Template::render(
        "stats",
        StatsTemplate {
            username: user.map(|user| user.username),
            year,
            votes1,
            voters1,
            votes2,
            voters2,
            ineligible_voters: ineligible_voters.into_iter().collect(),
            eligible_voters,
            blocked_voters,
            renamed,
            extra_r2_votes,
        },
    ))
}

#[derive(Serialize)]
struct SpamlistTemplate {
    username: Option<String>,
    targets_r1: usize,
    targets_r2: usize,
}

#[get("/spamlist")]
async fn spamlist_get(
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
    user: Option<UserGuard>,
) -> Result<Template> {
    let (year, _) = current_year(poty_state().await?);
    let (targets_r1, targets_r2) = tokio::try_join!(
        spamlist::spamlist_round1(commonsdb, toolsdb, year),
        spamlist::spamlist_round2(commonsdb, toolsdb, year),
    )?;
    Ok(Template::render(
        "spamlist",
        SpamlistTemplate {
            username: user.map(|user| user.username),
            targets_r1: targets_r1.len(),
            targets_r2: targets_r2.len(),
        },
    ))
}

#[post("/spamlist/<round>")]
async fn spamlist_post(
    round: usize,
    user: UserGuard,
    commonsdb: &State<CommonsDB>,
    toolsdb: &State<ToolsDB>,
    logger: &State<ConsoleSender>,
) -> Result<Json<bool>> {
    let (year, _) = current_year(poty_state().await?);
    spamlist::create(user, logger, commonsdb, toolsdb, year, round).await?;
    Ok(Json(true))
}

#[derive(Serialize)]
struct UploadersTemplate {
    username: String,
    templates: BTreeMap<String, String>,
    year: usize,
}

#[get("/uploaders")]
async fn uploaders_get(
    user: UserGuard,
    commonsdb: &State<CommonsDB>,
) -> Result<Template> {
    let (year, _) = current_year(poty_state().await?);
    let uploaders = uploaders::uploaders(year, commonsdb).await?;
    let templates = uploaders::templates(year, uploaders);
    Ok(Template::render(
        "uploaders",
        UploadersTemplate {
            username: user.username,
            templates,
            year,
        },
    ))
}

#[post("/uploaders")]
async fn uploaders_post(
    user: UserGuard,
    logger: &State<ConsoleSender>,
    commonsdb: &State<CommonsDB>,
) -> Result<Json<bool>> {
    let (year, _) = current_year(poty_state().await?);
    let uploaders = uploaders::uploaders(year, commonsdb).await?;
    let templates = uploaders::templates(year, uploaders);
    uploaders::send(user, logger, year, templates);
    Ok(Json(true))
}

#[derive(Serialize, Clone)]
struct ConsoleEvent {
    message: String,
    actor: String,
}

#[get("/events")]
async fn events(tx: &State<ConsoleSender>) -> EventStream![] {
    let mut rx = tx.subscribe();
    EventStream! {
        yield Event::json(&ConsoleEvent {
            message: "Connected.".to_string(),
            actor: "system".to_string(),
        });
        loop {
            let event = match rx.recv().await {
                Ok(event) => event,
                Err(RecvError::Closed) => break,
                Err(RecvError::Lagged(_)) => continue,
            };
            yield Event::json(&event);
        }
    }
}

#[get("/state.json")]
async fn state_json() -> Result<Json<POTYState>> {
    let state = poty_state().await?;
    Ok(Json(state))
}

#[catch(default)]
fn default_catcher(status: Status, _req: &Request) -> Template {
    Template::render(
        "error",
        ErrorTemplate {
            code: status.code,
            reason: status.reason_lossy(),
            error: None,
        },
    )
}

#[launch]
fn rocket() -> _ {
    mwbot::init_logging();
    rocket::build()
        .manage(broadcast::channel::<ConsoleEvent>(1024).0)
        .manage(CommonsDB(Pool::new(
            toolforge::connection_info!("commonswiki")
                .expect("failed to get db connection info")
                .to_string()
                .as_str(),
        )))
        .manage(ToolsDB(Pool::new(
            toolforge::db::toolsdb("s55366__poty_p".to_string())
                .expect("failed to get db connection info")
                .to_string()
                .as_str(),
        )))
        .attach(Template::fairing())
        .attach(Healthz::fairing())
        .attach(OAuth2::<MWOAuth>::fairing("wikimedia"))
        .mount(
            "/",
            routes![
                index,
                login,
                auth,
                events,
                init_get,
                init_create,
                candidates_get,
                candidates_create,
                catstats_get,
                catstats_get_year,
                checks_get,
                checks_get_year,
                votepages_get,
                votepages_post,
                stats_get,
                stats_year_get,
                spamlist_get,
                spamlist_post,
                uploaders_get,
                uploaders_post,
                uservotes_get,
                tally_r1_get,
                tally_r1_post,
                tally_r2_get,
                tally_r2_post,
                tally_r2_images,
                state_json,
            ],
        )
        .register("/", catchers![default_catcher])
        .mount("/static", FileServer::from("static"))
        .attach(AdHoc::on_liftoff("eligibility", |rocket| {
            // unwrap: Safe because we added these types a few lines above
            let toolsdb = rocket.state::<ToolsDB>().unwrap().clone();
            let commonsdb = rocket.state::<CommonsDB>().unwrap().clone();
            Box::pin(async move {
                tokio::spawn(async move {
                    eligibility::start_eligiblity_checking(toolsdb, commonsdb)
                        .await;
                });
            })
        }))
}
