use crate::guard::UserGuard;
use crate::{CommonsDB, ConsoleSender};
use anyhow::Result;
use mwbot::parsoid::WikinodeIterator;
use mwbot::SaveOptions;
use mysql_async::prelude::Queryable;
use std::collections::HashSet;

pub(crate) async fn existing(
    pool: &CommonsDB,
    year: usize,
    round: usize,
) -> Result<HashSet<String>> {
    let mut conn = pool.0.get_conn().await?;
    let pages = conn
        .exec(
            r#"
select page_title 
from page 
where page_namespace=4 
and page_title like ?
"#,
            (format!("Picture_of_the_Year/{year}/R{round}/v/%"),),
        )
        .await?
        .into_iter()
        .collect();
    Ok(pages)
}

pub(crate) fn create(
    user: UserGuard,
    sender: &ConsoleSender,
    year: usize,
    round: usize,
    existing: HashSet<String>,
) {
    let sender = sender.clone();
    tokio::spawn(async move {
        match create2(&user, &sender, year, round, existing).await {
            Ok(()) => {
                let _ = sender.send(user.console("Done!"));
            }
            Err(err) => {
                let _ = sender.send(user.console(&format!("Error: {err}")));
            }
        }
    });
}

async fn create2(
    user: &UserGuard,
    sender: &ConsoleSender,
    year: usize,
    round: usize,
    existing: HashSet<String>,
) -> Result<()> {
    let bot = user.bot().await?;
    let existing: HashSet<String> = existing
        .into_iter()
        .map(|title| {
            bot.page_from_database(4, &title)
                .expect("invalid title")
                .title()
                .to_string()
        })
        .collect();
    let page = bot.page(&match round {
        1 => format!("Commons:Picture of the Year/{year}/Candidates"),
        2 => {
            format!("Commons:Picture of the Year/{year}/Candidates/R2")
        }
        _ => panic!("Invalid round: {round}"),
    })?;
    let images: Vec<_> = page
        .html()
        .await?
        .into_mutable()
        .descendants()
        .filter_map(|node| node.as_gallery())
        .flat_map(|gallery| gallery.images())
        .map(|image| {
            format!(
                "Commons:Picture of the Year/{year}/R{round}/v/{}",
                image.title().strip_prefix("File:").unwrap()
            )
        })
        .collect();
    let opts = SaveOptions::summary("Creating POTY vote page");
    for image in images {
        if !existing.contains(&image) {
            let page = bot.page(&image)?;
            page.save(
                r#"
{{autotranslate|base=POTY/header|%%YEAR%%}}{{-}}{{POTY/Roundheader|%%YEAR%%}}

== {{int:Ratinghistory-table-votes}} ==
            "#
                .trim()
                .replace("%%YEAR%%", year.to_string().as_str()),
                &opts,
            )
            .await?;
            let _ = sender.send(user.console(&format!("Created [[{image}]]")));
        }
    }
    Ok(())
}
