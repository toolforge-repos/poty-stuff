// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
//! Stuff to initialize a new year

// TODO list:
// Commons:Picture of the Year/2021/MessageR2
// Commons:Picture of the Year/2021/Share

use crate::guard::UserGuard;
use crate::ConsoleSender;
use anyhow::{anyhow, Result};
use mwbot::{parsoid::prelude::*, SaveOptions};

pub(crate) struct Tokens {
    year: usize,
    current_year: usize,
    ordinal: String,
}

impl Tokens {
    pub(crate) fn new(year: usize) -> Self {
        let current_year = year + 1;
        let ordinal = num2words::Num2Words::new((year - 2005) as i32)
            .ordinal()
            .to_words()
            .expect("invalid ordinal thing");
        Self {
            year,
            current_year,
            ordinal,
        }
    }
}

pub(crate) async fn fetch(
    user: &UserGuard,
    tokens: &Tokens,
    logger: &ConsoleSender,
) -> Result<Vec<(String, String, bool)>> {
    let bot = user.bot().await?;
    let html = bot.page("Commons:Picture of the Year/Init")?.html().await?;
    let _ = logger.send(user.console(
        "Fetching and parsing [[Commons:Picture of the Year/Init]]...",
    ));
    let candidates = bot.page(&format!(
        "Commons:Picture of the Year/{}/Candidates",
        tokens.year
    ))?;
    let _ = logger.send(user.console(&format!(
        "Fetching and parsing [[{}]]...",
        candidates.title()
    )));
    let mut parsed = parse(html, tokens)?;
    match candidates.html().await {
        Ok(html) => parsed.extend(parse_candidates(html)?),
        Err(mwbot::Error::PageDoesNotExist(_)) => {}
        Err(err) => Err(err)?,
    };
    let mut create = vec![];
    for (title, wikitext) in parsed {
        let page = bot.page(&title)?;
        create.push((title, wikitext, page.exists().await?));
    }
    Ok(create)
}

fn parse_candidates(html: ImmutableWikicode) -> Result<Vec<(String, String)>> {
    let html = html.into_mutable();
    let links: Vec<_> = html
        .iter_sections()
        .into_iter()
        .filter_map(|section| section.heading())
        .filter_map(|heading| heading.filter_links().pop())
        .map(|link| (link.target(), "{{#invoke:POTY|gallery}}".to_string()))
        .collect();
    Ok(links)
}

fn parse(
    html: ImmutableWikicode,
    tokens: &Tokens,
) -> Result<Vec<(String, String)>> {
    let html = html.into_mutable();
    let mut create = vec![];
    for temp in html.filter_templates()? {
        if temp.name() == "Template:POTY/init" {
            let title = temp
                .param("1")
                .ok_or_else(|| anyhow!("missing 1= (title) parameter"))?;
            let wikitext = temp
                .param("2")
                .ok_or_else(|| anyhow!("missing 2= (content) parameter"))?;
            let wikitext = strip_pre_nowiki(wikitext)
                .ok_or_else(|| anyhow!("not properly wrapped in pre/nowiki"))?;
            let title = replace_tokens(&title, tokens);
            let wikitext = replace_tokens(&wikitext, tokens);
            create.push((title, wikitext));
        }
    }
    Ok(create)
}

pub(crate) async fn create(
    user: &UserGuard,
    tokens: &Tokens,
    logger: &ConsoleSender,
    pages: Vec<(String, String, bool)>,
) -> Result<()> {
    let bot = user.bot().await?;
    let opts =
        SaveOptions::summary(&format!("Initializing POTY {}", tokens.year));
    for (title, wikitext, exists) in pages {
        if exists {
            continue;
        }
        let page = bot.page(&title)?;
        page.save(wikitext, &opts).await?;
        let _ = logger.send(user.console(&format!("Created [[{title}]]")));
    }
    Ok(())
}

fn replace_tokens(input: &str, tokens: &Tokens) -> String {
    input
        .replace("%%YEAR%%", &tokens.year.to_string())
        .replace("%%CURRENTYEAR%%", &tokens.current_year.to_string())
        .replace("%%ORDINAL%%", &tokens.ordinal)
}

fn strip_pre_nowiki(text: String) -> Option<String> {
    Some(
        text.strip_prefix("<pre><nowiki>")?
            .strip_suffix("</nowiki></pre>")?
            .trim()
            .to_string(),
    )
}
