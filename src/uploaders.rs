use crate::{guard::UserGuard, CommonsDB, ConsoleSender};
use anyhow::Result;
use mwbot::SaveOptions;
use mysql_async::prelude::Queryable;
use std::collections::BTreeMap;
use tokio::time;

pub(crate) async fn uploaders(
    year: usize,
    pool: &CommonsDB,
) -> Result<BTreeMap<String, Vec<String>>> {
    let mut conn = pool.0.get_conn().await?;
    let rows: Vec<(String, String)> = conn
        .exec(
            r#"
select actor_name, page.page_title
from imagelinks
join page as page2 on page2.page_id = il_from
and page2.page_namespace = il_from_namespace
join page on page.page_title = il_to and page.page_namespace = 6
join revision_userindex on rev_page=page.page_id and rev_parent_id=0
join actor on rev_actor=actor_id
where
page2.page_title = ?
and page2.page_namespace = 4

            "#,
            (format!("Picture_of_the_Year/{year}/Candidates"),),
        )
        .await?;
    let mut map = BTreeMap::new();
    for (username, image) in rows {
        map.entry(username.to_string())
            .and_modify(|list: &mut Vec<String>| list.push(image.to_string()))
            .or_insert_with(|| vec![image]);
    }
    Ok(map)
}

pub(crate) fn templates(
    year: usize,
    uploaders: BTreeMap<String, Vec<String>>,
) -> BTreeMap<String, String> {
    let mut ret = BTreeMap::new();
    for (uploader, files) in uploaders {
        ret.insert(uploader, build_template(year, files));
    }
    ret
}

fn build_template(year: usize, files: Vec<String>) -> String {
    let mut temp = format!(
        "{{{{subst:Commons:Picture of the Year/{year}/Message/For uploaders|1="
    );
    for file in files {
        temp.push_str(&format!("* [[:File:{file}|{file}]]\n"));
    }
    temp.push_str("}}");
    temp
}

pub(crate) fn send(
    user: UserGuard,
    logger: &ConsoleSender,
    year: usize,
    templates: BTreeMap<String, String>,
) {
    let logger = logger.clone();
    tokio::spawn(async move {
        match send_real(&user, &logger, year, templates).await {
            Ok(()) => {}
            Err(err) => {
                let _ = logger.send(user.console(&format!("Error: {err}")));
            }
        }
    });
}

async fn send_real(
    user: &UserGuard,
    logger: &ConsoleSender,
    year: usize,
    templates: BTreeMap<String, String>,
) -> Result<()> {
    let bot = user.bot().await?;
    for (username, template) in templates {
        let page = bot.page(&format!("User talk:{username}"))?;
        page.save(
            template,
            &SaveOptions::summary(&format!("Your photos are in POTY {year}"))
                .section("new"),
        )
        .await?;
        let _ = logger.send(user.console(&format!("Notified {username}")));
        // Sleep two seconds between edits, so 30epm
        time::sleep(time::Duration::from_secs(2)).await;
    }
    Ok(())
}
