use crate::{commons_bot, CommonsDB};
use anyhow::Result;
use mysql_async::prelude::Queryable;
use rocket::form::validate::Contains;
use std::collections::BTreeSet;

pub(crate) async fn query_votes(
    pool: &CommonsDB,
    year: usize,
    username: &str,
) -> Result<(BTreeSet<String>, BTreeSet<String>)> {
    let userpage = commons_bot().await?.page(&format!("User:{username}"))?;
    let username = userpage.as_title().dbkey();
    let mut conn = pool.0.get_conn().await?;
    let title = format!("Picture_of_the_Year/{year}/R_/v/%");
    let found: Vec<String> = conn
        .exec(
            r#"
select
  page_title
from
  pagelinks
  join linktarget on pl_target_id = lt_id
  join page on pl_from = page_id
where
  page_namespace = 4
  and page_title like ?
  and lt_namespace = 2
  and pl_from_namespace = 4
  and lt_title = ?;
"#,
            (title, username),
        )
        .await?;
    let mut r1 = BTreeSet::new();
    let mut r2 = BTreeSet::new();
    for page in found {
        // unwrap: OK because the SQL verifies it has at least once slash
        let (_, title) = page.rsplit_once('/').unwrap();
        if page.contains("/R1/v/") {
            r1.insert(title.to_string());
        } else {
            r2.insert(title.to_string());
        }
    }
    Ok((r1, r2))
}
