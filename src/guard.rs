// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::config::committee;
use crate::{ConsoleEvent, USER_AGENT};
use anyhow::{anyhow, Error, Result};
use mwbot::Bot;
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome, Request};
use serde::{Deserialize, Serialize};
use tracing::debug;

#[derive(Serialize, Deserialize)]
pub struct UserGuard {
    pub username: String,
    pub oauth2_token: String,
}

impl UserGuard {
    pub async fn bot(&self) -> Result<Bot> {
        let bot = Bot::builder(
            "https://commons.wikimedia.org/w/api.php".to_string(),
            "https://commons.wikimedia.org/api/rest_v1".to_string(),
        )
        .set_user_agent(USER_AGENT.to_string())
        .set_oauth2_token(
            self.username.to_string(),
            self.oauth2_token.to_string(),
        )
        .set_save_delay(0)
        .set_maxlag(9999)
        .set_respect_nobots(false)
        .build()
        .await?;
        Ok(bot)
    }

    pub(crate) fn console(&self, msg: &str) -> ConsoleEvent {
        ConsoleEvent {
            message: msg.to_string(),
            actor: self.username.to_string(),
        }
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for UserGuard {
    type Error = Error;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let cookie = match req.cookies().get_private("username") {
            Some(cookie) => cookie,
            None => {
                return Outcome::Error((
                    Status::Unauthorized,
                    anyhow!("You are not logged in."),
                ))
            }
        };
        let guard: UserGuard = match serde_json::from_str(cookie.value()) {
            Ok(guard) => guard,
            Err(err) => {
                debug!(
                    "Invalid JSON in username cookie: {} ({})",
                    cookie.value(),
                    err
                );
                return Outcome::Error((
                    Status::Unauthorized,
                    anyhow!("You are not logged in."),
                ));
            }
        };
        if !committee()
            .await
            .unwrap_or_default()
            .contains(&guard.username)
        {
            return Outcome::Error((
                Status::Unauthorized,
                anyhow!("You are not on the POTY commitee"),
            ));
        }
        Outcome::Success(guard)
    }
}
