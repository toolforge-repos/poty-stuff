use crate::{commons_bot, CommonsDB};
use anyhow::Result;
use mwbot::parsoid::WikinodeIterator;
use mysql_async::prelude::Queryable;
use serde::Serialize;
use std::collections::HashSet;

pub(crate) async fn duplicates(year: usize) -> Result<Vec<String>> {
    let bot = commons_bot().await?;
    let page =
        bot.page(&format!("Commons:Picture of the Year/{year}/Candidates"))?;
    let mut seen = HashSet::new();
    let mut dupes = vec![];
    let html = page.html().await?.into_mutable();
    for image in html.filter_images() {
        let title = image.title();
        if seen.contains(&title) {
            dupes.push(title);
        } else {
            seen.insert(title);
        }
    }
    Ok(dupes)
}

pub(crate) async fn formerly_featured(
    pool: &CommonsDB,
    year: usize,
) -> Result<Vec<String>> {
    let mut conn = pool.0.get_conn().await?;
    let rows: Vec<String> = conn
        .exec(
            r#"
select
  page_title
from
  categorylinks
  join page on cl_from = page_id
where
  cl_to = "Formerly_featured_pictures_on_Wikimedia_Commons"
  and page_title IN (
    select
      il_to
    from
      imagelinks
      join page on page_id = il_from
      and page_namespace = il_from_namespace
    where
      page_title = ?
      and page_namespace = 4
  );
"#,
            (format!("Picture_of_the_Year/{year}/Candidates"),),
        )
        .await?;
    Ok(rows)
}

#[derive(Serialize, Debug)]
pub(crate) struct ImageResolution {
    width: u64,
    height: u64,
    ratio: String,
}

pub(crate) async fn pics_by_ratio(
    year: usize,
    pool: &CommonsDB,
) -> Result<Vec<(String, ImageResolution)>> {
    let mut conn = pool.0.get_conn().await?;
    let mut rows = conn
        .exec_map(
            r#"
select img_name, img_width, img_height
from image
join imagelinks on il_to=img_name
join page on page_id = il_from
and page_namespace = il_from_namespace
where
page_title = ?
and page_namespace = 4
"#,
            (format!("Picture_of_the_Year/{year}/Candidates"),),
            |(name, width, height): (String, u64, u64)| {
                (
                    name,
                    ImageResolution {
                        width,
                        height,
                        ratio: format!("{0:.2}", width as f64 / height as f64),
                    },
                )
            },
        )
        .await?;
    rows.sort_by_cached_key(|item| item.1.ratio.to_string());
    rows.reverse();
    Ok(rows)
}
