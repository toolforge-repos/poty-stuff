use crate::guard::UserGuard;
use crate::{CommonsDB, ConsoleSender, ToolsDB};
use anyhow::{anyhow, Result};
use mwbot::SaveOptions;
use mysql_async::prelude::Queryable;
use std::collections::{BTreeSet, HashSet};

async fn query_nonvoting_voters(
    commonsdb: &CommonsDB,
    voted_in: String,
    not_voted_in: String,
) -> Result<HashSet<String>> {
    let mut conn = commonsdb.0.get_conn().await?;
    let result: HashSet<String> = conn
        .exec(
            r#"
select DISTINCT lt_title
from pagelinks
join linktarget on pl_target_id=lt_id
join page on pl_from=page_id
join user on REPLACE(lt_title, '_', ' ')=user_name
left join ipblocks on user_id=ipb_user
where page_namespace=4
and page_title like ?
and lt_namespace=2
and pl_from_namespace=4
and ipb_id is null
and pl_target_id not in (
    select DISTINCT pl_target_id
    from pagelinks
    join page on pl_from=page_id
    where page_namespace=4
    and page_title like ?
    and pl_from_namespace=4
);
"#,
            (voted_in, not_voted_in),
        )
        .await?
        .into_iter()
        .collect();
    Ok(result)
}

/// Get the list of eligible year-1 voters who are not blocked and have not yet voted this year
pub(crate) async fn spamlist_round1(
    commonsdb: &CommonsDB,
    toolsdb: &ToolsDB,
    year: usize,
) -> Result<BTreeSet<String>> {
    // Get the list of year-1 voters who aren't blocked and have not voted this year
    let result = query_nonvoting_voters(
        commonsdb,
        format!("Picture_of_the_Year/{}/R_/v/%", year - 1),
        format!("Picture_of_the_Year/{year}/R1/v/%"),
    )
    .await?;

    // TODO: run this query concurrently with the above one
    let ineligible =
        crate::eligibility::ineligible_voters(toolsdb, year - 1).await?;
    let targets = result.difference(&ineligible).cloned().collect();
    Ok(targets)
}

/// Get the list of R1 voters who are not blocked and have not yet voted in R2
pub(crate) async fn spamlist_round2(
    commonsdb: &CommonsDB,
    toolsdb: &ToolsDB,
    year: usize,
) -> Result<BTreeSet<String>> {
    // Get the list of R1 voters who aren't blocked and have not voted in R2
    let result = query_nonvoting_voters(
        commonsdb,
        format!("Picture_of_the_Year/{year}/R1/v/%"),
        format!("Picture_of_the_Year/{year}/R2/v/%"),
    )
    .await?;

    // TODO: run this query concurrently with the above one
    let ineligible =
        crate::eligibility::ineligible_voters(toolsdb, year).await?;
    let targets = result.difference(&ineligible).cloned().collect();
    Ok(targets)
}

fn format_targets(targets: BTreeSet<String>) -> String {
    let mut list = vec![];
    for target in targets {
        list.push(format!("{{{{#target:User talk:{target}}}}}"))
    }
    list.join("\n")
}

pub(crate) async fn create(
    user: UserGuard,
    logger: &ConsoleSender,
    commonsdb: &CommonsDB,
    toolsdb: &ToolsDB,
    year: usize,
    round: usize,
) -> Result<()> {
    let bot = user.bot().await?;
    let (targets, title) = match round {
        1 => (
            spamlist_round1(commonsdb, toolsdb, year).await?,
            format!("Commons:Picture_of_the_Year/{year}/Message/Targets"),
        ),
        2 => (
            spamlist_round2(commonsdb, toolsdb, year).await?,
            format!("Commons:Picture_of_the_Year/{year}/MessageR2/Targets"),
        ),
        _ => Err(anyhow!("invalid round {round}"))?,
    };
    let wikitext = format_targets(targets);
    let page = bot.page(&title)?;
    page.save(
        wikitext,
        &SaveOptions::summary("Creating/updating POTY spamlist"),
    )
    .await?;
    let _ = logger.send(user.console(&format!("Saved [[{title}]]")));
    Ok(())
}
