use crate::commons_bot;
use anyhow::Result;
use mwbot::parsoid::WikinodeIterator;
use serde::Serialize;
use std::collections::{BTreeMap, HashMap};

#[derive(Serialize)]
pub(crate) struct CatStats {
    total: f32,
    mean: f32,
    std_deviation: f32,
    categories: BTreeMap<String, Cat>,
}

#[derive(Serialize)]
struct Cat {
    count: usize,
    z_score: f32,
}

pub(crate) async fn catstats(year: usize) -> Result<CatStats> {
    let bot = commons_bot().await?;
    let page =
        bot.page(&format!("Commons:Picture of the Year/{year}/Candidates"))?;
    let mut categories = HashMap::new();
    let html = page.html().await?.into_mutable();
    for section in html.iter_sections() {
        let heading = match section.heading() {
            Some(heading) => heading,
            None => {
                continue;
            }
        };
        let count = section
            .descendants()
            .filter_map(|node| node.as_gallery())
            .map(|gallery| gallery.images().len())
            .next()
            .unwrap_or(0);
        categories.insert(heading.text_contents(), count);
    }
    let total = categories.values().sum::<usize>() as f32;
    let mean = total / categories.len() as f32;
    // Standard deviation, roughly copied from <https://rust-lang-nursery.github.io/rust-cookbook/science/mathematics/statistics.html#standard-deviation>
    let std_deviation = {
        let variance = categories
            .values()
            .map(|val| {
                let diff = mean - (*val as f32);
                diff * diff
            })
            .sum::<f32>()
            / categories.len() as f32;
        variance.sqrt()
    };
    Ok(CatStats {
        total,
        mean,
        std_deviation,
        categories: categories
            .into_iter()
            .map(|(name, count)| {
                let z_score = (count as f32 - mean) / std_deviation;
                (name, Cat { count, z_score })
            })
            .collect(),
    })
}
