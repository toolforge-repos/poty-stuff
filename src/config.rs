use crate::commons_bot;
use anyhow::Result;
use cached::proc_macro::cached;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

pub type POTYState = BTreeMap<usize, Config>;

#[derive(Deserialize, Serialize, Clone)]
pub struct Config {
    /*
    // TODO: enum
    state: String,
    // TODO: dates
    r1start: String,
    r1end: String,
    r2start: String,
    r2end: String,
    */
    pub eligibility: EligibiltyCfg,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct EligibiltyCfg {
    pub id: usize,
    /*
    link: String,
    edits: usize,
    date: String,
     */
}

#[cached(sync_writes = true, result = true, time = 300)]
pub(crate) async fn poty_state() -> Result<POTYState> {
    let bot = commons_bot().await?;
    let json = bot.page("Module:POTY/state.json")?.wikitext().await?;
    let state: POTYState = serde_json::from_str(&json)?;
    Ok(state)
}

pub(crate) fn current_year(mut state: POTYState) -> (usize, Config) {
    let year = *state.keys().max().unwrap();
    (year, state.remove(&year).unwrap())
}

#[cached(sync_writes = true, result = true, time = 300)]
pub(crate) async fn committee() -> anyhow::Result<Vec<String>> {
    let page = commons_bot().await?.page("MediaWiki:POTY/committee.json")?;
    let mut json: serde_json::Value =
        serde_json::from_str(&page.wikitext().await?)?;
    let members = serde_json::from_value(json["members"].take())?;
    Ok(members)
}
