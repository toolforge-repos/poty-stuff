poty-stuff
==========

This is the admin tool for administering the Commons Picture of the Year competition.

## Developing locally

1. Install the [`toolforge-tunnel` helper](https://docs.rs/toolforge/latest/toolforge/#toolforge-tunnel-helper) and run `toolforge-tunnel commonswiki` to open a database tunnel.
2. Copy `Rocket.toml.example` to `Rocket.toml`, create your own
OAuth consumer to fill in (or ask Legoktm for his test tool). Generate the `default.secret` key with something like `openssl rand -base64 32`.
3. Run with `cargo run` and visit http://localhost:8000`. You should be able to login and run most queries.
   * Changes to templates will automatically take effect when you reload the page, but changes to Rust code will require restarting the `cargo run` process.

There will be some errors related to accessing the `s55366__poty_p` database, I don't have a solution for doing that locally yet.

## Deploying

Clone Legoktm's [toolforge-ansible](https://gitlab.wikimedia.org/legoktm/toolforge-ansible) repository and run:

```
$ ansible-playbook -i hosts.yaml playbook.yaml --limit poty-stuff
```
