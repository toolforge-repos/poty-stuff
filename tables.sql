CREATE TABLE eligibility (
	e_id int not null AUTO_INCREMENT,
	e_username varchar(1000) not null,
	e_year int not null,
	e_eligible bool not null,
	PRIMARY KEY (e_id),
	UNIQUE KEY (e_username, e_year)
)
CHARACTER SET 'utf8mb4';
