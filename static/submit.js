const elems = document.getElementsByClassName("submit");
Array.from(elems).forEach(function(elem) {
	elem.addEventListener("click", function(event) {
		const target = this.dataset.target;
		fetch(target, {method: "POST"})
	});
});
