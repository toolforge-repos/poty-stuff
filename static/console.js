const evtSource = new EventSource("/events");
evtSource.onmessage = (event) => {
	let ul = document.getElementById("console");
	const data = JSON.parse(event.data);
	let li = document.createElement("li");
	li.append(`[${data.actor}] ${data.message}`)
	ul.prepend(li);
};
